use std::io::{self, Write};
use std::str::Split;


//1. shell built in commands : exit, cd, pwd, echo
//  tester la navigation dans le shell : détecter l'utilisateur actuelle, où il se trouve
//  afficher un prompt avec user@hostname

enum Command {
    exit,
    cd,
    pwd,
    echo
}


fn get_shell_builtin_cmd(command_in : &str) -> Result<Option<Command>, &'static str> {
    match command_in {
        "exit" => Ok(Some(Command::exit)),
        "cd" => Ok(Some(Command::cd)),
        "pwd" => Ok(Some(Command::pwd)),
        "echo" => Ok(Some(Command::echo)),
        "" => Ok(None),
        _ => Err("command not found"),
    }
}

fn exec_shell_builtin_cmd(cmd : Command, split_cmd : Vec<&str>) {
    //println!("Exécution de la commande : {}", split_cmd.);
    match cmd {
        Command::cd => println!("cd"),
        Command::echo => println!("echo"),
        Command::pwd => println!("pwd"),
        Command::exit => println!("exit")
    }
}

fn main() {
    let mut quit = false;

    while !quit {

        let mut input = String::new();

        print!("RSH>");
        let _ = io::stdout().flush();
        io::stdin().read_line(&mut input).expect("Error reading from STDIN");

        //get the first word of the input which is the command
        let command_in = input.split_whitespace().next().unwrap_or("");

        let command = get_shell_builtin_cmd(command_in);

        //gestion des 3 cas
        match command {
            //commande reconnue
            Ok(Some(c)) => exec_shell_builtin_cmd(c, input.split(" ").collect()),
            //pas de commande renseignée
            Ok(None) => print!(""),
            //commande invalide
            Err(e) => println!("error : {} {e:?}", command_in)
        }
        

    }
}
